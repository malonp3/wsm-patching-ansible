#############################################################################
#       Author: Mayank Agarwal
#       Reviewer:    
#       Date: 05/29/2020
#       Description: Domain Controllers HealthCheck Status
#############################################################################

$Header = @"
<style>
TABLE {border-width: 1px; border-style: solid; border-color: black; border-collapse: collapse;}
TH {border-width: 1px; padding: 3px; border-style: solid; border-color: black; background-color: #6495ED;}
TD {border-width: 1px; padding: 3px; border-style: solid; border-color: black;}
</style>
"@

$smtphost = "mapintmail.bsci.bossci.com" 
$from = "DoNotReply@bsci.com" 
$email1 = "GWIS-Server-offshore@bsci.com"

$getForest = [system.directoryservices.activedirectory.Forest]::GetCurrentForest()
$DCServers = $getForest.domains | ForEach-Object {$_.DomainControllers} | ForEach-Object {$_.Name}
$Results = @()
foreach ($DC in $DCServers){
if ( Test-Connection -ComputerName $DC -Count 1 -ErrorAction SilentlyContinue ) {
$PingStatus = "Success"
$NetlogonService = Get-Service -ComputerName $DC -Name Netlogon | Select -ExpandProperty Status
$NTDSService = Get-Service -ComputerName $DC -Name NTDS | Select -ExpandProperty Status
$DNSService = Get-Service -ComputerName $DC -Name DNS | Select -ExpandProperty Status


####################Netlogons status##################

$sysvol = Invoke-Command -ComputerName $DC -ScriptBlock { dcdiag /test:Netlogons }
if($sysvol -match "passed test netlogons")
{
$Netlogons = "Passed"
}
Else
{
$Netlogons = "Failed"
}

####################Replications status##################

$sysvol = Invoke-Command -ComputerName $DC -ScriptBlock { dcdiag /test:Replications }
if($sysvol -match "passed test Replications")
{
$Replication = "Passed"
}
Else
{
$Replication = "Failed"
}


####################Services status##################

$sysvol = Invoke-Command -ComputerName $DC -ScriptBlock { dcdiag /test:Services }
if($sysvol -match "passed test Services")
{
$Services = "Passed"
}
Else
{
$Services = "Failed"
}


####################Advertising status##################

$sysvol = Invoke-Command -ComputerName $DC -ScriptBlock { dcdiag /test:Advertising }
if($sysvol -match "passed test Advertising")
{
$Advertising = "Passed"
}
Else
{
$Advertising = "Failed"
}

####################FSMOCheck status##################

$sysvol = Invoke-Command -ComputerName $DC -ScriptBlock { dcdiag /test:FSMOCheck }
if($sysvol -match "passed test FsmoCheck")
{
$FSMO = "Passed"
}
Else
{
$FSMO = "Failed"
}


#####################Uptime############################


$LBUT = Get-WmiObject -ComputerName $DC -Query "SELECT LastBootUpTime FROM Win32_OperatingSystem"
$Uptime = [System.Management.ManagementDateTimeConverter]::ToDateTime($LBUT.LastBootUpTime)
}

Else
{
$PingStatus = "Failed"
$NetlogonService = "Failed"
$NTDSService = "Failed"
$DNSService = "Failed"
$Netlogons = "Failed"
$Replication = "Failed"
$Services = "Failed"
$Advertising = "Failed"
$FSMO = "Failed"
$Uptime = "Failed"
}

$Properties = @{
DCName = $DC
PingStatus = $PingStatus
NetlogonService = $NetlogonService
NTDSService = $NTDSService
DNSService = $DNSService
Netlogons = $Netlogons
Replication = $Replication
Services = $Services
Advertising = $Advertising
FSMO = $FSMO
LastBootTime = $Uptime
}
$Results += New-Object psobject -Property $Properties

}

$Results | Select-Object DCName,PingStatus,NetlogonService,NTDSService,DNSService,Netlogons,Replication,Services,Advertising,FSMO,LastBootTime | ConvertTo-Html -Title "DC Health Check" -Head $Header | foreach {
    $PSItem -replace "<td>Running</td>", "<td style='background-color:#6DE825'>Running</td>" `
    -replace "<td>Stopped</td>", "<td style='background-color:#F00C0C'>Stopped</td>" `
    -replace "<td>Failed</td>", "<td style='background-color:#F00C0C'>Failed</td>" `
    -replace "<td>Success</td>", "<td style='background-color:#6DE825'>Success</td>" `
    -replace "<td>Passed</td>", "<td style='background-color:#6DE825'>Passed</td>"
} | Out-File C:\temp\ansible\ReportDCHealth.htm

$subject = "Domain Controller Health Check"
$body = Get-Content "C:\temp\ansible\ReportDCHealth.htm" 
$smtp= New-Object System.Net.Mail.SmtpClient $smtphost 
$msg = New-Object System.Net.Mail.MailMessage 
$msg.To.Add($email1)
$msg.cc.Add("guillaume.duverchin@bsci.com")
$msg.from = $from
$msg.subject = $subject	 
$msg.body = $body 
$msg.isBodyhtml = $true 
$smtp.send($msg)
