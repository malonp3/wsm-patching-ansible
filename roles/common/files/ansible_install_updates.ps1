$result = @()
$computername = Get-WMIObject Win32_ComputerSystem | Select-Object -ExpandProperty name
Write-Host $computername
$UpdatesToInstall = Get-WmiObject -ComputerName $computername -query "select * from ccm_softwareupdate" -Namespace "root\ccm\clientsdk"
$ApplicationToInstall = Get-CimInstance -ClassName CCM_Application -Namespace "root\ccm\clientSDK" -ComputerName $Computername | Where-Object {$_.installstate -like 'NotInstalled'}

foreach ($App in $ApplicationToInstall){
    #$Application = Get-CimInstance -ClassName CCM_Application -Namespace "root\ccm\clientSDK" -ComputerName $Computername | Where-Object {$_.installstate -like 'NotInstalled'}

    $Args = @{EnforcePreference = [UINT32] 0
        Id = "$($App.id)"
        IsMachineTarget = $App.IsMachineTarget
        IsRebootIfNeeded = $False
        Priority = 'High'
        Revision = "$($App.Revision)"
       }

    $App | Invoke-CimMethod -name Install -Arguments $Args
    # Invoke-CimMethod -Namespace "root\ccm\clientSDK" -ClassName CCM_Application -ComputerName $computername Install -Arguments $Args

}

[System.Management.ManagementObject[]] $CMMissingUpdates = @(GWMI -ComputerName $computername -query "SELECT * FROM CCM_SoftwareUpdate WHERE ComplianceState = '0'" -namespace "ROOT\ccm\ClientSDK") #End Get update count.
$result += "The number of missing updates is $($CMMissingUpdates.count)"

$updates = $CMMissingUpdates.count

If ($updates) {
   #Install the missing updates.
   $CMInstallMissingUpdates = (GWMI -ComputerName $computername -Namespace "ROOT\ccm\ClientSDK" -Class "CCM_SoftwareUpdatesManager" -List).InstallUpdates($CMMissingUpdates)
   Do {
      Start-Sleep -Seconds 15
      [array]$CMInstallPendingUpdates = @(GWMI -ComputerName $computername -query "SELECT * FROM CCM_SoftwareUpdate WHERE EvaluationState = 6 or EvaluationState = 7" -namespace "ROOT\ccm\ClientSDK")
      Write-Progress -Activity "Updates are installing..." -PercentComplete (($CMInstallPendingUpdates.count/$updates)*100)
   } While (($CMInstallPendingUpdates.count -gt 0))
} Else {
   $result += "There are no missing updates to be Installed."
}

# Create Completion File
$Date = Get-Date -format yyyyMMdd
Add-Content c:\temp\Update_Completed.txt "Update Completed at $Date"

# Remove Update Task
Unregister-ScheduledTask -TaskName "Run Ansible Update Script" -Confirm:$false
