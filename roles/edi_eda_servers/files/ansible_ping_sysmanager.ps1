Function Run-SCCMClientAction {
  [CmdletBinding()]

  # Parameters used in this function
  param
  (
      [Parameter(Position=0, Mandatory = $True, HelpMessage="Provide server names", ValueFromPipeline = $true)]
      [string[]]$Computername,

     [ValidateSet('MachinePolicy',
                  'DiscoveryData',
                  'ComplianceEvaluation',
                  'AppDeployment',
                  'HardwareInventory',
                  'UpdateDeployment',
                  'UpdateScan',
                  'SoftwareInventory',
                  'AppEvaluation')]
      [string[]]$ClientAction

  )
  $ActionResults = @()
  Try {
          $ActionResults = Invoke-Command -ComputerName $Computername {param($ClientAction)

                  Foreach ($Item in $ClientAction) {
                      $Object = @{} | select "Action name",Status
                      Try{
                          $ScheduleIDMappings = @{
                              'MachinePolicy'        = '{00000000-0000-0000-0000-000000000021}';
                              'DiscoveryData'        = '{00000000-0000-0000-0000-000000000003}';
                              'ComplianceEvaluation' = '{00000000-0000-0000-0000-000000000071}';
                              'AppDeployment'        = '{00000000-0000-0000-0000-000000000121}';
                              'HardwareInventory'    = '{00000000-0000-0000-0000-000000000001}';
                              'UpdateDeployment'     = '{00000000-0000-0000-0000-000000000108}';
                              'UpdateScan'           = '{00000000-0000-0000-0000-000000000113}';
                              'SoftwareInventory'    = '{00000000-0000-0000-0000-000000000002}';
                              'AppEvaluation'        = '{00000000-0000-0000-0000-000000000123}';

                            }
          $ScheduleID = $ScheduleIDMappings[$item]
                          Write-Verbose "Processing $Item - $ScheduleID"
                          [void]([wmiclass] "root\ccm:SMS_Client").TriggerSchedule($ScheduleID);
                          $Status = "Success"
                          Write-Verbose "Operation status - $status"
                      }
                      Catch{
                          $Status = "Failed"
                          Write-Verbose "Operation status - $status"
                      }
                      $Object."Action name" = $item
                      $Object.Status = $Status
                      $Object
                  }

      } -ArgumentList $ClientAction -ErrorAction Stop | Select-Object @{n='ServerName';e={$_.pscomputername}},"Action name",Status
  }
  Catch{
      Write-Error $_.Exception.Message
  }
  Return $ActionResults
}


# Run System Manager Processes, to pull any new updates
# From: https://www.powershellbros.com/sccm-client-actions-remote-machines-powershell-script/

$ComputerName = Get-WMIObject Win32_ComputerSystem | Select-Object -ExpandProperty name
Write-Host $computername

# Multiple actions with verbose mode on this server
$RetVal_1 = Run-SCCMClientAction -Computername $ComputerName -ClientAction AppDeployment -Verbose

$RetVal_2 = Run-SCCMClientAction -Computername $ComputerName -ClientAction UpdateDeployment -Verbose

#$RetVal_3 = Run-SCCMClientAction -Computername $ComputerName -ClientAction AppEvaluation -Verbose

# Sleep for a few minutes
Start-Sleep -Seconds 180

# Create Completion File
$Date = Get-Date -format yyyyMMddmmss
Add-Content C:\temp\ansible\SoftwareCenterPing.txt "Update Completed at $Date"
Add-Content C:\temp\ansible\SoftwareCenterPing.txt "====================================="
Add-Content C:\temp\ansible\SoftwareCenterPing.txt "$RetVal_1"
Add-Content C:\temp\ansible\SoftwareCenterPing.txt "====================================="
Add-Content C:\temp\ansible\SoftwareCenterPing.txt "$RetVal_2"
#Add-Content C:\temp\ansible\SoftwareCenterPing.txt "====================================="
#Add-Content C:\temp\ansible\SoftwareCenterPing.txt "$RetVal_3"
Add-Content C:\temp\ansible\SoftwareCenterPing.txt "//////////////////////////////////////////////////////////////////////////"
