$result = @()
$computername = Get-WMIObject Win32_ComputerSystem | Select-Object -ExpandProperty name
Write-Host $computername
$UpdatesToInstall = Get-WmiObject -ComputerName $computername -query "select * from ccm_softwareupdate" -Namespace "root\ccm\clientsdk"
Write-Host $UpdatesToInstall

[System.Management.ManagementObject[]] $CMMissingUpdates = @(GWMI -ComputerName $computername -query "SELECT * FROM CCM_SoftwareUpdate WHERE ComplianceState = '0'" -namespace "ROOT\ccm\ClientSDK") #End Get update count.
$result += "The number of missing updates is $($CMMissingUpdates.count)"
Write-Host $result

$updates = $CMMissingUpdates.count

If ($updates) {
  Write-Host $CMMissingUpdates
  #
  Write-Host (Get-CMSoftwareUpdate -Id $SoftwareUpdate).LocalizedDisplayName
}