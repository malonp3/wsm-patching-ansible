param ([string]$ServerInstanceName = $(throw "-ServerInstanceName is required."))

$ReturnFlag = "false"
$LogPath = "C:\temp\SQL_Connectivity_Check_$($ServerInstanceName.Replace("\","-"))_$((Get-Date).toString("MMddyyyyHHmmss")).log"

try {
    $Results = Invoke-Sqlcmd -Query "select @@servername" -ServerInstance $ServerInstanceName -Database "master" -ErrorAction Stop -QueryTimeout 120
    if (!$Results) {
        Add-Content -Path $LogPath -Value "No Results"
        $ReturnFlag = "true"
    }
    Remove-Variable Results
} catch {
    Add-Content -Path $LogPath -Value $_.Exception.Message
    $ReturnFlag = "true"
}

Remove-Variable ServerInstanceName
Remove-Variable LogPath

return $ReturnFlag
